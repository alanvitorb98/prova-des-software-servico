package prova_des_soft_servico.demo.Produto;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
@Autowired ProdutoRepository produtoRepository;


@PostMapping("/criar")
    public Produto criarProduto(@RequestBody Produto produto) {
    return produtoRepository.save(produto);
}

@GetMapping("/lista")
    public List<Produto> listarProdutos() {
    return produtoRepository.findAll();
}

@PutMapping("atualizar/{id}")
    public Produto atualizarProduto(@PathVariable Long id, @RequestBody Produto produto) {
    produto.setId(id);
    return produtoRepository.save(produto);

}
@DeleteMapping("deletar/{id}")
    public void deletarProduto(@PathVariable Long id) {
    produtoRepository.deleteById(id);
}






}
