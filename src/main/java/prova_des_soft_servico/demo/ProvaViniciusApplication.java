package prova_des_soft_servico.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaViniciusApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaViniciusApplication.class, args);
	}

}
